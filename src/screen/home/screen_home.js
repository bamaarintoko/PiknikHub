/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry, ImageBackground,
    StyleSheet,
    View
} from 'react-native';
import {connect} from "react-redux";
import {ListView, TouchableWithoutFeedback} from 'react-native';
import {
    Body,
    Button, Container, Content, Fab, Footer, FooterTab, Form, Header, Input, Item, Left, Right, SwipeRow,
    Text, Thumbnail, Title
} from "native-base";
import axios from 'axios'
import color from '../../utils/Constant/Color/Style'
import Icon from 'react-native-vector-icons/FontAwesome';
import {Home, Location, Profile, Pesan} from './Component'

const plesir = [
    {
        name:'Bambang',
        image: require('../assets/panorama/indrayanti.png'),
        destination: 'Pantai Indrayanti, DI Yogykarta, Indonesia',
        date: '1 Desember 2017',
        hours: '10:00',
        meet_poing: 'Sleman, Yogyakarta',
        capacity: 10,
        val: 5,
        create_date:'5 menit yang lalu'
    },
    {
        name : 'Sylvi',
        image: require('../assets/panorama/jungwok.png'),
        destination: 'Pantai Jungwok, DI Yogykarta, Indonesia',
        date: '1 Desember 2017',
        hours: '10:00',
        meet_poing: 'Sleman, Yogyakarta',
        capacity: 10,
        val: 5,
        create_date:'5 menit yang lalu'
    },
    {
        name:'Tree',
        image: require('../assets/panorama/indrayanti.png'),
        destination: 'Pantai Indrayanti, DI Yogykarta, Indonesia',
        date: '1 Desember 2017',
        hours: '10:00',
        meet_poing: 'Sleman, Yogyakarta',
        capacity: 10,
        val: 5,
        create_date:'5 menit yang lalu'
    }
    ,
    {
        name : 'Bayu',
        image: require('../assets/panorama/merbabu.png'),
        destination: 'Gunung Merbabu, DI Yogykarta, Indonesia',
        date: '1 Desember 2017',
        hours: '10:00',
        meet_poing: 'Sleman, Yogyakarta',
        capacity: 10,
        val: 5,
        create_date:'5 menit yang lalu'
    }
    ,
    {
        name : 'Edi',
        image: require('../assets/panorama/andong.png'),
        destination: 'Gunung Andong, DI Yogykarta, Indonesia',
        date: '1 Desember 2017',
        hours: '10:00',
        meet_poing: 'Sleman, Yogyakarta',
        capacity: 10,
        val: 5,
        create_date:'5 menit yang lalu'
    }
]

class screen_home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            int: 0,
            tab_nav: 'home',
            active: true
        }

    }

    componentWillMount() {

    }

    render() {
        console.log(this.state.tab_nav)
        return (
            <Container style={{backgroundColor: '#F0F0F0'}}>
                <Header style={color.headerLognRegis}>
                    <Body style={{flex: 4}}>
                    <Title style={{fontSize: 14, textAlign: 'center'}}>Semua Rute Piknik</Title>
                    </Body>
                    <Right style={{flex: 1}}>
                        <Button full transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon size={20} name="search" color="#FFFFFF"/>
                        </Button>
                    </Right>
                </Header>

                <Content>
                    <View style={{padding: 10}}>

                        {
                            plesir.map((x, i) => {
                                return (

                                    <View key={i} style={{marginTop: 5, marginBottom: 5}}>

                                        <TouchableWithoutFeedback key={i}
                                                                  onPress={() => this.props.navigation.navigate('Detail')}>
                                            <View style={{flex: 1, height:140, flexDirection:'column', borderWidth:1, borderColor:'#BDBDBD' }}>
                                                <View style={{flex:1, paddingTop:5, flexDirection:'row',backgroundColor:'#FFFFFF',justifyContent: 'center',
                                                    alignItems: 'center'}}>
                                                    <View style={{flex:1, paddingLeft:15}}>
                                                        <Thumbnail source={require('../assets/user.jpg')} style={{height:30, width:30}}  />
                                                    </View>
                                                    <View style={{flex:3, backgroundColor:'#FFFFFF',justifyContent: 'center',}}>
                                                        <Text style={{fontSize:12, paddingLeft:10}}>{x.name}</Text>
                                                    </View>
                                                    <View style={{flex:3, justifyContent: 'center', paddingRight:15}}>
                                                        <Text style={{fontSize:10, paddingLeft:10, textAlign:'right'}}>{x.create_date}</Text>
                                                    </View>
                                                </View>
                                                <View style={{flex: 1, backgroundColor: '#FFFFFF', padding: 15, borderBottomWidth:1, borderBottomColor:'#BDBDBD'}}>
                                                    <Text style={{fontSize: 10}}><Icon color={'#424242'}
                                                                                       name="map-marker"
                                                                                       size={13}/>{'    '}Dari {x.meet_poing}
                                                    </Text>
                                                    <Text style={{
                                                        fontSize: 10,
                                                        color: '#424242',
                                                        textAlign: 'left',
                                                        paddingRight: 10,
                                                        marginTop:10
                                                    }}><Icon color={'#424242'} name="location-arrow"
                                                             size={13}/>{'  '}{x.destination}</Text>

                                                </View>
                                                <View style={{flex: 1, backgroundColor: '#FFFFFF', flexDirection:'row',paddingLeft:15,justifyContent: 'center',
                                                    alignItems: 'center'}}>

                                                        <View style={{flex: 1}}>
                                                            <Text style={{fontSize: 10}}><Icon color={'#BDBDBD'} name="calendar-check-o"
                                                                     size={12}/>{'  '}{x.date}{'        '}</Text>
                                                        </View>
                                                    <View style={{flex:1}}>
                                                        <Text style={{fontSize: 10}}>
                                                            <Icon
                                                                color={'#BDBDBD'} name="clock-o"
                                                                size={12}/>{'  '}{x.hours}
                                                        </Text>
                                                    </View>
                                                        <View style={{flex: 1, paddingBottom: 4}}>
                                                            <Text style={{fontSize: 10}}><Icon color={'#BDBDBD'}
                                                                                               name="user-o"
                                                                                               size={13}/>{'   '}{x.val}
                                                                dari {x.capacity} orang</Text>
                                                        </View>

                                                </View>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    </View>
                                )
                            })
                        }

                    </View>

                </Content>
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#22a7f0',marginBottom:50 }}
                    position="bottomRight"
                    onPress={() => this.props.navigation.navigate('AddAgenda')}>
                    <Icon name="plus" />

                </Fab>
                <Footer>
                    <FooterTab style={{backgroundColor: '#FFFFFF'}}>
                        <Button onPress={() => this.setState({tab_nav: 'home'})}>
                            <Icon color={this.state.tab_nav === 'home' ? '#22a7f0' : '#424242'} name="home"
                                  size={this.state.tab_nav === 'home' ? 23 : 20}/>
                        </Button>
                        <Button onPress={() => this.setState({tab_nav: 'location'})}>
                            <Icon color={this.state.tab_nav === 'location' ? '#22a7f0' : '#424242'} name="map-marker"
                                  size={this.state.tab_nav === 'location' ? 23 : 20}/>
                        </Button>
                        <Button onPress={() => this.setState({tab_nav: 'pesan'})}>
                            <Icon color={this.state.tab_nav === 'pesan' ? '#22a7f0' : '#424242'} name="envelope-o"
                                  size={this.state.tab_nav === 'pesan' ? 23 : 20}/>
                        </Button>
                        <Button onPress={() => {
                            this.setState({tab_nav: 'profile'});
                            this.props.navigation.dispatch({type: 'onDetail'})
                        }}>
                            <Icon color={this.state.tab_nav === 'profile' ? '#22a7f0' : '#424242'} name="user"
                                  size={this.state.tab_nav === 'profile' ? 23 : 20}/>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

export default connect()(screen_home)
