/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry, ImageBackground,
    StyleSheet,
    View
} from 'react-native';
import {connect} from "react-redux";
import {ListView, TouchableWithoutFeedback} from 'react-native';
import {
    Body, Button, Container, Content, Footer, FooterTab, Form, Header, Input, Item, Left, Right, SwipeRow,
    Text, Thumbnail, Title
} from "native-base";
import axios from 'axios'
import color from '../../utils/Constant/Color/Style'
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal'
import styles_ from '../../utils/Constant/app.style';
import Api from "../../utils/Api";
class screen_detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            int: 0,
            show:false
        }

    }

    show() {
        this.setState({show: true})
    }

    hide() {
        this.setState({show: false})
    }
    componentWillMount() {

    }

    onJoin(){
        this.hide()
        // const params = {
        //     agenda_id : 1,
        //     user_id : 1
        // }
        // Api.POST('auth', params)
        //     .then((response) => {
        //         console.log(response.data)
        //     })
        //     .catch(error => console.log(error))
    }
    render() {
        //console.log(this.state.data)
        return (
            <Container>
                <Header style={color.headerLognRegis}>
                    <Left style={{flex: 1}}>
                        <Button full transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon size={20} name="arrow-left" color="#FFFFFF"/>
                        </Button>
                    </Left>
                    <Body style={{flex: 4}}>
                    <Title style={{fontSize: 14}}>Detail Rute Piknik</Title>
                    </Body>
                    <Right style={{flex: 1}}>
                        <Button transparent>
                            <Icon size={20} name="comments" color="#FFFFFF"/>
                        </Button>
                    </Right>
                </Header>
                <Modal isVisible={this.state.show} style={styles_.bottomModal} onBackButtonPress={()=>console.log("back brooo!!")}>
                    <View style={styles_.modalContent}>
                        <View style={{flex:1, flexDirection:'row'}}>
                            <View style={{flex:1}}>
                                <Button full onPress={()=>this.onJoin()}>
                                    <Text>Join</Text>
                                </Button>
                            </View>
                            <View style={{flex:1}}>
                                <Button full onPress={()=>this.hide()}>
                                    <Text>Batal</Text>
                                </Button>
                            </View>
                        </View>
                    </View>
                </Modal>
                <Content>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <View style={{height: 125, marginTop: 5, marginBottom: 5}}>
                            <View style={{flex: 1}}>
                                <ImageBackground resizeMode="contain"
                                                 source={require('../assets/panorama/andong.jpg')}
                                                 style={{
                                                     flex: 1,
                                                     alignSelf: 'stretch',
                                                     width: undefined,
                                                     height: undefined,
                                                     flexWrap: 'wrap',
                                                     flexDirection: 'column',
                                                 }}>

                                </ImageBackground>
                            </View>
                        </View>
                        <View style={{height: 110, marginTop: 5, backgroundColor: '#FFFFFF'}}>
                            <Text style={{
                                paddingTop: 10,
                                paddingLeft: 15,
                                fontSize: 12,
                                color: '#424242',
                                textAlign: 'left'
                            }}><Icon color={'#424242'} name="location-arrow" size={13}/>{'  '}Gunung Andong, Jawa
                                Tengah, Indonesia</Text>
                            <Text style={{
                                paddingLeft: 15,
                                marginTop: 5,
                                fontSize: 10,
                                color: '#424242',
                                textAlign: 'left'
                            }}><Icon color={'#424242'} name="calendar-check-o" size={12}/>{'  '}1 Desember
                                2017 {'        '}<Icon color={'#424242'} name="clock-o" size={12}/>{'  '} 10:00</Text>


                            <Text style={{
                                paddingLeft: 15,
                                marginTop: 20,
                                fontSize: 10,
                                color: '#424242',
                                textAlign: 'left'
                            }}>Dari</Text>
                            <View style={{flex: 1, flexDirection: 'row'}}>
                                <View style={{flex: 1}}>
                                    <Text style={{
                                        paddingLeft: 15,
                                        marginTop: 5,
                                        fontSize: 10,
                                        color: '#424242',
                                        textAlign: 'left'
                                    }}>Sleman, Yogyakarta</Text>
                                </View>
                                <View style={{flex: 1}}>
                                    <Text style={{
                                        paddingLeft: 15,
                                        marginTop: 5,
                                        fontSize: 10,
                                        color: '#424242',
                                        textAlign: 'left'
                                    }}><Icon color={'#424242'} name="map-marker" size={12}/>{'  '}Lihat lokasi meeting
                                        point</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{height: 30,backgroundColor: '#F0F0F0'}}>
                            <Text style={{fontSize:14, color:'#BDBDBD'}}>INFO DETAIL</Text>
                        </View>
                        <View style={{height: 170, backgroundColor: '#FFFFFF'}}>
                            <View style={{flex:1, flexDirection:'row'}}>
                                <View style={{flex:1, marginLeft:15}}>
                                    <Text style={{fontSize:12, color:'#BDBDBD', paddingTop:5}}>Jenis kendaraan</Text>
                                    <Text style={{fontSize:10, color:'#000000', paddingTop:5}}>BUS</Text>

                                    <Text style={{fontSize:12, color:'#BDBDBD', paddingTop:5}}>Jumlah Kuota Tersedia</Text>
                                    <Text style={{fontSize:10, color:'#000000', paddingTop:5}}>2 dari 10 Orang</Text>

                                    <Text style={{fontSize:12, color:'#BDBDBD', paddingTop:5}}>Biaya patungan / Orang</Text>
                                    <Text style={{fontSize:10, color:'#000000', paddingTop:5}}>Rp 100.000</Text>

                                    <Text style={{fontSize:12, color:'#BDBDBD', paddingTop:5}}>Keterangan lain</Text>
                                    <Text style={{fontSize:10, color:'#000000', paddingTop:5}}>DIlarang mencret dijalan ya gays</Text>
                                </View>

                                <View style={{flex:1}}>
                                    <Text style={{fontSize:12, color:'#BDBDBD'}}>Fasilitas</Text>
                                    <Text style={{fontSize:10, color:'#000000', paddingTop:5}}>- Makan bareng isyana</Text>
                                    <Text style={{fontSize:10, color:'#000000', paddingTop:5}}>- Dapat pembalut</Text>
                                    <Text style={{fontSize:10, color:'#000000', paddingTop:5}}>- Air Minum</Text>
                                    <Text style={{fontSize:10, color:'#000000', paddingTop:5}}>- Nasi Telur</Text>
                                </View>

                            </View>
                        </View>
                        <View style={{height: 30,backgroundColor: '#F0F0F0'}}>
                            <Text style={{fontSize:14, color:'#BDBDBD'}}>Dibuat Oleh</Text>
                        </View>
                        <View style={{backgroundColor: '#FFFFFF', padding:10, marginBottom:10}}>
                            <View style={{flex:1, flexDirection:'row'}}>
                                <View style={{flex:1}}>
                                <Thumbnail source={require('../assets/user.jpg')} />
                                </View>
                                <View style={{flex:4}}>
                                    <Text style={{fontSize:14}}>
                                        Rizky Kurniawan
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </Content>
                <Footer>
                    <FooterTab>
                        <Button style={{backgroundColor:'#22a7f0'}} onPress={()=>this.show()}>
                            <Text style={{fontSize:14, fontWeight:'bold',color:'#FFFFFF'}}>Ikut Piknik</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

export default connect()(screen_detail)
