/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View
} from 'react-native';
import {connect} from "react-redux";
import {ListView, TouchableWithoutFeedback} from 'react-native';
import {Button, Container, Content, Footer, FooterTab, Form, Header, Input, Item, SwipeRow, Text} from "native-base";
import axios from 'axios'

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            int: 0
        }

    }

    componentWillMount() {

    }

    render() {
        //console.log(this.state.data)
        return (
            <View>
                <Text>
                    Profile
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

export default connect()(Profile)
