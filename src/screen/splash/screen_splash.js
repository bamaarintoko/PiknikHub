/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry, Image,
    StyleSheet,
    Text,
    View
} from 'react-native';
import {connect} from "react-redux";
import * as Animatable from 'react-native-animatable';
import Swiper from 'react-native-swiper'
import {Button, Container, Content, Input, Item} from "native-base";

import color from '../../utils/Constant/Color/Style'

let styles = {
        container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
    },
    wrapper: {
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF',
        padding:10
    },
    text: {
        color: '#000000',
        fontSize: 30,
        fontWeight: 'bold'
    }
};

// import img from '../../utils/piknik_logo.png'


class screen_splash extends Component {
    constructor(){
        super();
        this.state= {
            isLoading : true
        }
    }
    componentDidMount(){
        setTimeout(() => {
            console.log("halooooo");
            this.setState({
                isLoading:false
            })
            //this.props.navigation.dispatch({type: 'Home'});
        }, 5000)
    }
    onLogin(){
        this.props.navigation.dispatch({type: 'onLogin'})
        console.log("login berooo!")
    }
    onRegister(){
        this.props.navigation.dispatch({type: 'onRegister'})
        console.log("register berooo!")
    }
    render() {
        console.log("---->",this.state.isLoading)
        return (
            <Container>
                {


        !this.state.isLoading
            ?
            <View style={{backgroundColor: '#fff',flex: 1,
                flexDirection: 'column',
                justifyContent: 'space-between'}}>
                <View style={{flex:7}}>
                <Swiper showButtons loadMinimal loadMinimalSize={1} loop={false}>

                <Animatable.View style={styles.slide1} animation="bounceInUp" easing="ease-out" iterationCount={this.state.isLoading ? "infinite" : 1 }>

                <View style={styles.slide1}>
                    <Text style={styles.text}>Hello Swiper</Text>
                </View>
                </Animatable.View>
                <View style={styles.slide2}>
                    <Text style={styles.text}>Beautiful</Text>
                </View>
                    <View style={styles.slide3}>
                        <Image source={require('../../utils/piknik_logo.png')} style={{width: 100, height:100, marginBottom:100}}/>
                        {/*<Item regular>*/}
                            {/*<Input placeholder='Username' />*/}
                        {/*</Item>*/}
                        {/*<Item regular style={{marginTop:10}}>*/}
                            {/*<Input placeholder='Password' />*/}
                        {/*</Item>*/}
                        {/*<Button full light rounded style={{marginTop:10}}>*/}
                            {/*<Text>Login</Text>*/}
                        {/*</Button>*/}
                    </View>


            </Swiper>
                </View>
                <View style={{flex:2}}>
                    <Button full light rounded style={color.primary} onPress={()=>this.onLogin()}>
                        <Text style={{fontWeight:'bold', color:'#FFFFFF'}}>Masuk</Text>
                    </Button>
                    <Button full light rounded style={color.primary} onPress={()=>this.onRegister()}>
                        <Text style={{fontWeight:'bold', color:'#FFFFFF'}}>Daftar Sekarang</Text>
                    </Button>
                </View>
            </View>
            :<View style={styles.container} height={200}>
                <Animatable.Text animation={this.state.isLoading ? "pulse" : "bounceOutUp"} easing="ease-out" iterationCount={this.state.isLoading ? "infinite" : 1 } style={{ textAlign: 'center' }}>
                    <Image source={require('../../utils/piknik_logo.png')} style={{width: 200, height:200}}/>
                </Animatable.Text>


            </View>
                }
            </Container>


        );
    }
}

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//         backgroundColor: '#F0F0F0',
//     },
//     welcome: {
//         fontSize: 20,
//         textAlign: 'center',
//         margin: 10,
//     },
//     instructions: {
//         textAlign: 'center',
//         color: '#333333',
//         marginBottom: 5,
//     },
// });

export default connect()(screen_splash)
