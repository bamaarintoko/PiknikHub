import React,{Component} from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View } from 'react-native';
import color from '../utils/Constant/Color/Style'
import {Header, Left, Button, Body, Title, Right} from "native-base";
import Icon from 'react-native-vector-icons/FontAwesome';
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
});

class Header_ extends Component{
    render(){
    //console.log("--->",props)


    return (
        <Header style={color.headerLognRegis}>
            <Left>
                <Button full transparent onPress={() => this.props.navigation.goBack()}>
                    <Icon size={20} name="arrow-left" color="#FFFFFF"/>
                </Button>
            </Left>
            <Body>
            <Title>{this.props.title}</Title>
            </Body>
            <Right>
                <Button transparent>

                </Button>
            </Right>
        </Header>
    )}
};

// LoginScreen.propTypes = {
//     navigation: PropTypes.object.isRequired,
// };
//
// LoginScreen.navigationOptions = {
//     title: 'Log In',
// };

export default Header_;