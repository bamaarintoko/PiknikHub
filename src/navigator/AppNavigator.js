import React, {Component} from 'react';
//import PropTypes from 'prop-types';
import {
    AppRegistry, Button,
    StyleSheet,
    Text,
    View,
    BackHandler
} from 'react-native';
import screenHome from '../screen/home/screen_home'
import screenList from '../screen/home/Component/Home'
import screenSplash from '../screen/splash/screen_splash'
import screenAdd from '../screen/home/screen_add'
import screenDetail from '../screen/home/screen_detail'
import screen_add_agenda from '../screen/home/screen_add_agenda'
import screenAddDet from '../screen/detail/screen_add_det'
import screenLogin from '../screen/login/screen_login'
import screenRegister from '../screen/register/screen_register'

import {addNavigationHelpers, DrawerNavigator, StackNavigator} from "react-navigation";
import {connect} from "react-redux";
import {Container, Content, List, ListItem, Separator} from "native-base";


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        backgroundColor: '#f0f0f0',
        height: 100
    }
});
class CustomDrawerContentComponent extends Component {

    onBack() {
        this.props.navigation.navigate('DrawerClose')
    }

    render() {

        return (
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Button
                        onPress={() => {
                            this.onBack();
                            this.props.navigation.dispatch({type: 'LoginScreen'})
                        }}
                        title="Log in"
                    />
                </View>
                <Container>
                    <Content>
                        <List>
                            <Separator bordered>
                            </Separator>
                            <ListItem onPress={() => {
                                this.onBack();
                                this.props.navigation.dispatch({type: 'LoginScreen'});
                            }}>
                                <Text>Pesanan</Text>
                            </ListItem>
                            <ListItem onPress={() => {
                                this.onBack();
                                this.props.navigation.dispatch({type: 'Event'})
                            }}>
                                <Text>Event</Text>
                            </ListItem>
                            <Separator bordered>
                            </Separator>
                            <ListItem onPress={() => {
                                this.onBack();
                                this.props.navigation.dispatch({type: 'Konfirmasi'})
                            }}>
                                <Text>Konfirmasi Pembelian</Text>
                            </ListItem>
                            <ListItem onPress={() => {
                                this.onBack();
                                this.props.navigation.dispatch({type: 'TokoFavorit'})
                            }}>
                                <Text>Toko Favorit</Text>
                            </ListItem>
                            <ListItem onPress={() => {
                                this.onBack();
                                this.props.navigation.dispatch({type: 'WishList'})
                            }}>
                                <Text>Wish List</Text>
                            </ListItem>
                            <ListItem onPress={() => {
                                this.onBack();
                                this.props.navigation.dispatch({type: 'RedeemVoucher'})
                            }}>
                                <Text>Redeem Voucher</Text>
                            </ListItem>
                            <ListItem onPress={() => {
                                this.onBack();
                                this.props.navigation.dispatch({type: 'Alamat'})
                            }}>
                                <Text>Alamat</Text>
                            </ListItem>
                            <ListItem onPress={() => {
                                this.onBack();
                                this.props.navigation.dispatch({type: 'CustomerService'})
                            }}>
                                <Text>Customer Service</Text>
                            </ListItem>
                            <ListItem onPress={() => {
                                this.onBack();
                                this.props.navigation.dispatch({type: 'Setting'})
                            }}>
                                <Text>Setting</Text>
                            </ListItem>
                        </List>
                    </Content>
                </Container>
            </View>
        )
    }


}
const sideBar = DrawerNavigator({
    Home: {screen: screenHome},

}, {
    contentComponent: CustomDrawerContentComponent
});
export const AppNavigator = StackNavigator({
    Splash: {screen: screenSplash},
    Login: {screen: screenLogin},
    Register: {screen: screenRegister},
    //Menu : {screen:sideBar},
    Home : {screen:screenHome},
    List : {screen:screenList},
    Add : {screen:screenAdd},
    Detail : {screen:screenDetail},
    AddAgenda : {screen:screen_add_agenda},
    AddDet : {screen:screenAddDet},

}, {
    headerMode: 'none'
});
class AppWithNavigationState extends Component{
    constructor(props) {
        super(props);
    }
    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', function() {
            const { dispatch, navigation, nav } = this.props;
            console.log("-->",nav.routes.length)

            if (nav.routes.length === 1 && (nav.routes[0].routeName === 'Home')) {
                BackHandler.exitApp()
                return false;
            }
            dispatch({ type: 'Navigation/BACK' });
            return true;
        }.bind(this));
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress');
    }
    render(){
        return(
            <AppNavigator navigation={addNavigationHelpers({ dispatch: this.props.dispatch, state: this.props.nav })}/>
        )
    }
};

const mapStateToProps = state => ({
    nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);