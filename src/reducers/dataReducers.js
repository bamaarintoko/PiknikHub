import { NavigationActions } from 'react-navigation';

import { AppNavigator } from '../navigator/AppNavigator';

const firstAction = AppNavigator.router.getActionForPathAndParams('Splash');

const initialNavState = AppNavigator.router.getStateForAction(
    firstAction,

);

export function nav(state = initialNavState, action) {
    let nextState;

    console.log(action.type);
    switch (action.type) {
        case 'onLoginSuccess':
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.reset({
                    index:0,
                    actions : [
                        NavigationActions.navigate({routeName: 'Home'})
                    ]}),
                state
            );
            break;

        case 'LOG_OUT':
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.reset({
                    index:0,
                    actions : [
                        NavigationActions.navigate({routeName: 'Splash'})
                    ]}),
                state
            );
            break;
        case 'LOGIN_SUCCESS':
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.reset({
                    index:0,
                    actions : [
                        NavigationActions.navigate({routeName: 'Splash'})
                    ]}),
                state
            );
            break;
        case 'two':

            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'screenTwo' }),
                state
            );
            break;
        case 'onLogin':
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'Login' }),
                state
            );
            break;
        case 'NAV_DETAIL_PROD':

            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'screenOne' }),
                state
            );
            break;
        case 'NAV_FILTER_RESPONSES':

            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'screenFilterResponses' }),
                state
            );
            break;
        case 'three':

            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'screenThree' }),
                state
            );
            break;
        case 'four':

            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'screenFour' }),
                state
            );
            break;


        default:
            nextState = AppNavigator.router.getStateForAction(action, state);
            break;

            console.log(state)
    }
    return nextState || state;
}